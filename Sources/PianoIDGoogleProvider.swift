import UIKit

import PianoOAuth

import GoogleSignIn

public class PianoIDGoogleProvider: PianoIDSocialProvider {
    
    public override var name: String { "google" }
    
    public override func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        return GoogleSignIn.GIDSignIn.sharedInstance.handle(url)
    }
    
    public override func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any?) -> Bool {
        return GoogleSignIn.GIDSignIn.sharedInstance.handle(url)
    }
    
    public override func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GoogleSignIn.GIDSignIn.sharedInstance.handle(url)
    }
    
    public override func signIn(controller: UIViewController, completion: @escaping (Bool, String?, Error?) -> Void) {
        DispatchQueue.main.async {
            GoogleSignIn.GIDSignIn.sharedInstance.signIn(withPresenting: controller) { result, error in
                if let e = error {
                    if let ge = e as? GIDSignInError, ge.code == GIDSignInError.canceled {
                        completion(true, nil, nil)
                        return
                    }

                    completion(false, nil, e)
                    return
                }
                
                guard let u = result?.user, let t = u.idToken else {
                    completion(false, nil, PianoIDError.googleSignInFailed)
                    return
                }

                completion(false, t.tokenString, nil)
            }
        }
    }
}
