// swift-tools-version:5.10

import PackageDescription

let package = Package(
    name: "PianoOAuthGoogle",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "PianoOAuthGoogle",
            targets: ["PianoOAuthGoogle"]
        )
    ],
    dependencies: [
        .package(url: "https://github.com/google/GoogleSignIn-iOS", .upToNextMinor(from: "8.0.0")),
        .package(url: "https://gitlab.com/piano-public/sdk/ios/package", .upToNextMinor(from: "2.8.6"))
    ],
    targets: [
        .target(
            name: "PianoOAuthGoogle",
            dependencies: [
                .product(name: "GoogleSignIn", package: "GoogleSignIn-iOS"),
                .product(name: "PianoOAuth", package: "package")
            ],
            path: "Sources"
        ),
        .testTarget(
            name: "PianoOAuthGoogleTests",
            dependencies: ["PianoOAuthGoogle"],
            path: "Tests"
        )
    ]
)
