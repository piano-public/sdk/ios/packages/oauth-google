# Piano OAuth integration with Google for iOS

[![Version](https://img.shields.io/cocoapods/v/PianoOAuthGoogle.svg?style=flat)](http://cocoapods.org/pods/PianoOAuthGoogle)
[![Platform](https://img.shields.io/cocoapods/p/PianoOAuthGoogle.svg?style=flat)](http://cocoapods.org/pods/PianoOAuthGoogle)
[![License](https://img.shields.io/cocoapods/l/PianoOAuthGoogle.svg?style=flat)](http://cocoapods.org/pods/PianoOAuthGoogle)

## Requirements
- iOS 16.0+
- Xcode 15.3
- Swift 5.10 (iOS target version 12+)

## Installation

### [CocoaPods](https://cocoapods.org/)

Add the following lines to your `Podfile`.

```
use_frameworks!

pod 'PianoOAuthGoogle', '~> 2.8.6'
```

Then run `pod install`. For details of the installation and usage of CocoaPods, visit [official web site](https://cocoapods.org/).

### [Swift Package Manager](https://developer.apple.com/documentation/swift_packages/adding_package_dependencies_to_your_app)
Add the component `PianoOAuthGoogle` from the repository:

**URL:** https://gitlab.com/piano-public/sdk/ios/packages/oauth-google

**Version:** 2.8.6

## PianoOAuthGoogle Usage

### Imports
```swift
import PianoOAuthGoogle
```

### Configuration
You should configure your application as described here: https://developers.google.com/identity/sign-in/ios/start-integrating

### Integration
Add before the `PianoIDApplicationDelegate.shared.application` function call
```swift
// iOS 14+
@main
struct MyApp: App {
    init() {
        PianoID.shared.add(socialProvider: PianoIDGoogleProvider())
    }
}

// iOS 13 and earlier versions
class AppDelegate: UIResponder, UIApplicationDelegate {
    override init() {
        super.init()
        PianoID.shared.add(socialProvider: PianoIDGoogleProvider())
    }
    ...
}
```
